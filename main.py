import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import parser

fig = plt.figure()
ax = plt.axes(xlim=(-10, 10), ylim=(-10, 10))

line, = ax.plot([], [], lw=1)
line2, = ax.plot([], [], lw=1,color='b')
xdata, ydata = [], []
x1data, y1data = [], []

def init():
    line.set_data([], [])
    line2.set_data([], [])
    return line,line2

def cartesian_coords_of_curve(t):
    x = eval(parsed_x)
    y = eval(parsed_y)
    return x, y

def differentiation(t):
    eps = 0.001
    d1 = cartesian_coords_of_curve(t)
    d2 = cartesian_coords_of_curve(t + eps)
    dx = (d2[0] - d1[0]) / eps
    dy = (d2[1] - d1[1]) / eps
    return dx, dy

while True:
    try:
        print("Podaj funkcję x dla równania parametrycznego:")
        input_x = input()
        parsed_x = parser.expr(input_x).compile()
        print("Podaj funkcję y dla równania parametrycznego:")
        input_y = input()
        parsed_y = parser.expr(input_y).compile()
        print("Podaj r koła:")
        r = float(input())
        test = cartesian_coords_of_curve(1)
        if not ('t' in input_x and 't' in input_y):
            print('debug')
            raise ValueError('Równanie parametryczne - proszę użyć zmiennej t')
        break
    except:
        print("Wprowadzono błędne dane")
        continue

argument = np.arange(0 , 2 * np.pi, 0.01)
x_f, y_f = cartesian_coords_of_curve(argument)
length = np.sum(np.sqrt(np.power(x_f, 2) + np.power(y_f, 2))) * 0.01
freq = (length/(2 * np.pi * r)) + 1

def animate(i):
    t = 0.02 * i
    x_f, y_f = cartesian_coords_of_curve(t)
    dx, dy = differentiation(t)

    # Oblicz środek krzywej
    x_center = np.mean(x_f)
    y_center = np.mean(y_f)
    
    xd1 = x_center - r * dy/(np.sqrt(np.power(dx, 2)+np.power(dy, 2)))
    yd1 = y_center + r * dx/(np.sqrt(np.power(dx, 2)+np.power(dy, 2)))
    xd2 = x_center + ((r * dy) / (np.sqrt(np.power(dx, 2) + np.power(dy, 2))))
    yd2 = y_center - ((r * dx) / (np.sqrt(np.power(dx, 2) + np.power(dy, 2))))
    
    x1 = xd1 - r * np.sin(freq * t + np.pi / 2)
    y1 = yd1 - r * np.cos(freq * t + np.pi / 2)
    x2 = xd2 + r * np.sin(freq * t - np.pi / 2)
    y2 = yd2 - r * np.cos(freq * t - np.pi / 2)

    xdata.append(x1)
    ydata.append(y1)
    x1data.append(x2)
    y1data.append(y2)
    line.set_data(xdata, ydata)
    line2.set_data(x1data, y1data)
    circle1 = plt.Circle((xd1, yd1), r, fill=False)
    circle2 = plt.Circle((xd2, yd2), r, fill=False)
    ax.add_patch(circle1)
    ax.add_patch(circle2)
    return line,line2, circle1, circle2

animacja = animation.FuncAnimation(fig, animate, init_func=init, frames=20000, interval=50, blit=True)
plt.show()
