# README

## Projekt: Wizualizacja krzywych w przestrzeni z użyciem Matplotlib

Ten projekt jest przeznaczony do wizualizacji krzywych opisanych równaniami parametrycznymi, a także do wyświetlania stycznych kręgów o określonym promieniu wzdłuż tych krzywych.

### Wymagania

Do uruchomienia tego projektu potrzebujesz:

- Python 3.7+
- matplotlib 3.1+
- numpy 1.16+
- matplotlib.animation

### Jak korzystać?

1. Uruchom skrypt Pythona.

2. Kiedy zostaniesz poproszony, wprowadź równanie x(t) i y(t) dla krzywej parametrycznej. Pamiętaj, aby użyć 't' jako parametru.

3. Następnie wprowadź promień 'r' dla stycznych okręgów.

4. Po wprowadzeniu tych danych, skrypt rozpocznie generowanie animacji krzywej wraz z stycznymi okręgami.

### Przykład

Przykładowe dane, które możesz wprowadzić:

- Funkcja x: `t`
- Funkcja y: `t**2`
- Promień: `1`

Powinno to wygenerować animację parabolii z kręgami stycznymi.

### Informacje dodatkowe

Proszę pamiętać, że ten skrypt generuje animację w czasie rzeczywistym, więc może wymagać dużych zasobów systemowych dla skomplikowanych równań.
